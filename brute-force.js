export default function bruteForce(A, B, I) {
    const matchedPairs = []
    A.forEach(aItem => {
        B.forEach(bItem => {
            if (aItem + bItem === I)
                matchedPairs.push([aItem, bItem])
        })
    })
    return matchedPairs;
}