
# Array Sum Matching

Given two arrays A and B of dimension N and an integer I,
return an array containing all the pairs of one element in A and one element in B
which sums equals I

## *Example 1* :

A = [1, 2, 4, 3]

B = [8, 14, 50, 1]

I =  10

**Out = [[2, 8]]**

## *Example 2* :

A = [1, 8, 5]

B = [2, 7, 4]

I =  11

**Out = [[8, 2], [8, 4], [5, 7]]**
