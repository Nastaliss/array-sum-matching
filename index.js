import { performance } from 'perf_hooks';
import bruteForce from "./brute-force";


/* Array Sum Matching

Given two arrays A and B of dimension N and an integer I,
return an array containing all the pairs of one element in A and one element in B
which sums equals I
Example 1 :

A = [1, 2, 4, 3]
B = [8, 14, 50, 1]
I =  10

Out = [[2, 8]]

Example 2 :

A = [1, 8, 5]
B = [2, 7, 4]
I =  11

Out = [[8, 2], [8, 4], [5, 7]]
*/


// Util

// const variableToString = variable => Object.keys(variable)[0];
const printSolutions = (algorithms, array1, array2, integer) => console.log([algorithms.map((algorithmsFunction, i) => {
    const solutionStart = performance.now()
    const solution = algorithmsFunction(array1, array2, integer)
    const solutionEnd = performance.now()
    return {
        name: `solution ${i + 1}`, checks: solution === verifier, time: solutionEnd - solutionStart
    }
})])


// Input values
const A = [1, 2, 4, 3]
const B = [8, 14, 6, 1]
const I = 10

// Random test
const randomTestLength = 20;
const maxAB = 50;
const maxI = 100;
const aGenerator = () => [...Array(randomTestLength)].map((_) => Math.round(Math.random() * maxAB))
const bGenerator = () => [...Array(randomTestLength)].map((_) => Math.round(Math.random() * maxAB))
const iGenerator = () => Math.round(Math.random() * maxI)

//
// ─── YOUR CODE HERE ─────────────────────────────────────────────────────────────
//


const solutions = [
    bruteForce,
]

//
// ─── /YOUR CODE HERE ─────────────────────────────────────────────────────────────
//

// User test
const verifier = bruteForce(A, B, I)

printSolutions(solutions, A, B, I)



// Results
solutions.forEach((solution) => {
    console.log()
})


